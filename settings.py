from os import environ
from pathlib import Path

from dotenv import load_dotenv

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

DOCKER_FLAG = environ.get("DOCKER_FLAG")
API_PORT = environ.get("API_PORT")
MONGO_URL = environ.get("MONGO_URL_PROD")
MONGO_PORT = environ.get("MONGO_PORT_DEFAULT")
