from tornado.escape import json_encode
from tornado.testing import AsyncHTTPTestCase

from main import ApplicationTornado
from src.exceptions.colour_not_available_required_exception import ColourNotAvailableException
from src.exceptions.guess_colours_required_exception import ColorInputRequiredException
from src.exceptions.username_not_found_exception import UserNameNotFoundException
from src.exceptions.username_required_exception import UserNameRequiredException
from src.models.attempted_guess import AttemptedGuess
from src.models.code import Code
from src.models.game import Game
from src.models.player import Player


class BackendTest(AsyncHTTPTestCase):

    def get_app(self):
        return ApplicationTornado()

    def test_invalid_url(self):
        response = self.fetch('/example', method='POST', body='')
        self.assertEqual(response.code, 404)
        self.assertEqual(response.reason, 'Invalid resource path')

    def test_create_game_empty_body(self):
        body = json_encode({})
        response = self.fetch('/createGame', method='POST', body=body)
        self.assertEqual(response.code, 400)
        self.assertEqual(response.reason, 'Username is required')

    def test_create_game_username_empty(self):
        body = json_encode({'username': ''})
        response = self.fetch('/createGame', method='POST', body=body)
        self.assertEqual(response.code, 400)
        self.assertEqual(response.reason, 'Username is required')

    def test_play_game_username_empty(self):
        body = json_encode({'username': ''})
        response = self.fetch('/play', method='POST', body=body)
        self.assertEqual(response.code, 400)
        self.assertEqual(response.reason, 'Username is required')

    def test_play_game_guess_colour_empty(self):
        body = json_encode({'username': 'example'})
        response = self.fetch('/play', method='POST', body=body)
        self.assertEqual(response.code, 400)
        self.assertEqual(response.reason, 'Colour input is required')

    def test_player_with_username(self):
        player = Player("playername")
        self.assertEqual("playername", player.username)

    def test_player_without_username(self):
        with self.assertRaises(UserNameRequiredException):
            Player()

    def test_game_without_player(self):
        with self.assertRaises(UserNameNotFoundException):
            Game()

    def test_game_filled(self):
        player = Player("playername")
        game = Game(player=player)
        self.assertEqual("playername", game.player.username)
        self.assertEqual(0, game.attempts)
        self.assertEqual([], game.attempted_guess)
        self.assertNotEqual(None, game.secret_code)

    def test_wrong_code(self):
        with self.assertRaises(ColourNotAvailableException):
            Code("AAAA")

    def test_good_code(self):
        code = Code("YYYY")
        self.assertEqual(list("YYYY"), code.colours)

    def test_less_digit_code(self):
        with self.assertRaises(ColorInputRequiredException):
            Code("YYY")

    def test_attempted_guess(self):
        guess_colour = "YYYY"
        attempted_guess = AttemptedGuess(guess_colour, 0, 0)
        self.assertEqual(guess_colour, attempted_guess.guess_code)
        self.assertEqual(0, attempted_guess.exact)
        self.assertEqual(0, attempted_guess.partial)

