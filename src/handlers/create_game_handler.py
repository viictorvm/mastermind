from tornado import escape

from src.exceptions.username_required_exception import UserNameRequiredException
from src.handlers.custom_request_handler import CustomRequestHandler
from src.services.create_game_service import CreateGameService
from src.utils.is_empty import is_empty


class CreateGameHandler(CustomRequestHandler):

    def post(self):
        data = escape.json_decode(self.request.body)
        username = data.get('username', None)
        if is_empty(username):
            raise UserNameRequiredException
        create_game_service = CreateGameService()
        status_code = 200
        status_msg = "Game created"
        msg_result = create_game_service.create_game(username)
        self.set_status(status_code, status_msg)
        self.write({'result': msg_result})
        self.finish()
