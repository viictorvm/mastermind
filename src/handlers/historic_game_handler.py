from src.handlers.custom_request_handler import CustomRequestHandler

from src.exceptions.username_required_exception import UserNameRequiredException
from src.services.historic_game_service import HistoricGameService
from src.utils.is_empty import is_empty


class HistoricGame(CustomRequestHandler):

    def get(self, username):
        if is_empty(username):
            raise UserNameRequiredException
        historic_game_service = HistoricGameService()
        result = historic_game_service.find_historic_game_by_username(username)
        self.write(result)
        self.finish()
