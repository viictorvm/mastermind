from json import dumps

from tornado import escape

from src.exceptions.guess_colours_required_exception import ColorInputRequiredException
from src.exceptions.username_required_exception import UserNameRequiredException
from src.handlers.custom_request_handler import CustomRequestHandler
from src.services.play_game_service import PlayGameService
from src.utils.is_empty import is_empty


class PlayGameHandler(CustomRequestHandler):

    def post(self):
        play_game_service = PlayGameService()
        data = escape.json_decode(self.request.body)
        username = data.get('username', None)
        guess = data.get('guess', None)
        if is_empty(username):
            raise UserNameRequiredException
        if is_empty(guess):
            raise ColorInputRequiredException
        result = play_game_service.exec_game(username, guess)
        self.set_status(200, "Played")
        self.write(dumps(result))
        self.finish()
