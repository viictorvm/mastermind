from src.models.game import Game
from src.repository.base_repository import BaseRepository


class GameRepository(BaseRepository):

    def __init__(self):
        super(GameRepository, self).__init__()
        self.db = self.db.mastermind

    def persist(self, game: Game):
        return self.db.insert_one(game.to_json())

    def find_game_by_player(self, player):
        result = self.db.find_one(player.to_json())
        if result:
            return Game(player=player, attempts=result.get('attempts'), secret_code=result.get('secret_code'),
                        attempted_guess=result.get('attempted_guess'), found=result.get('found'))
        return None

    def add_attempted_guess_by_player(self, player, id_attempted_guess, found):
        return self.db.update_one({'username': player.username},
                                  {
                                      '$push': {'attempted_guess': id_attempted_guess},
                                      '$inc': {'attempts': 1},
                                      '$set': {'found': found}
                                  })
