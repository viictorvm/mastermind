from src.models.attempted_guess import AttemptedGuess
from src.repository.base_repository import BaseRepository
from src.services.guess_checker_service import GuessChecker


class AttemptedGuessRepository(BaseRepository):

    def __init__(self):
        super(AttemptedGuessRepository, self).__init__()
        self.db = self.db.attempted_guess

    def persist(self, guess_checker: GuessChecker):
        return self.db.insert(guess_checker.to_json())

    def find_attempted_guess_by_id(self, attempted_guess_id):
        result = self.db.find_one({'_id': attempted_guess_id}, {'_id': 0})
        if result:
            guess_colour = ''.join(result.get('guess_colours'))
            return AttemptedGuess(
                guess_colour=guess_colour,
                exact=result.get('exact'),
                partial=result.get('partial')
            )

        return None
