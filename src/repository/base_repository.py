from src.utils.db import Connect


class BaseRepository:
    db = None

    def __init__(self):
        self.db = Connect.getConnection()
        if self.db:
            self.db = self.db
