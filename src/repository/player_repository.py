from src.models.player import Player
from src.repository.base_repository import BaseRepository


class PlayerRepository(BaseRepository):

    def __init__(self):
        super(PlayerRepository, self).__init__()
        self.db = self.db.player

    def persist(self, player):
        default_status = dict(player.to_json(), **{'deleted': False})
        return self.db.insert_one(default_status)

    def find_player_by_username(self, username):
        result = self.db.find_one({'username': username, 'deleted': False})
        if not result:
            return None
        return Player(username)
