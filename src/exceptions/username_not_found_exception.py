from tornado.web import HTTPError


class UserNameNotFoundException(HTTPError):
    def __init__(self):
        super().__init__(400, reason="Username not registered")
