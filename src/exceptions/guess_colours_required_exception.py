from tornado.web import HTTPError


class ColorInputRequiredException(HTTPError):
    def __init__(self):
        super().__init__(status_code=400, reason="Colour input is required")
