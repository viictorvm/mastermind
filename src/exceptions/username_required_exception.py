from tornado.web import HTTPError


class UserNameRequiredException(HTTPError):
    def __init__(self):
        super().__init__(status_code=400, reason="Username is required")
