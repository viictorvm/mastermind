from tornado.web import HTTPError


class ColourNotAvailableException(HTTPError):
    def __init__(self):
        super().__init__(400, reason="Colour not available yet!")
