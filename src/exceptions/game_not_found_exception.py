from tornado.web import HTTPError


class GameNotFoundException(HTTPError):
    def __init__(self):
        super().__init__(status_code=400, reason="Game Not Found!")

