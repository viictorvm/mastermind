from tornado.web import HTTPError


class RepeatedUserNameException(HTTPError):
    def __init__(self):
        super().__init__(status_code=400, reason="Username already registered!")
