from pymongo import MongoClient

import settings


class Connect(object):
    @staticmethod
    def getConnection():
        MONGO_URL = settings.MONGO_URL
        MONGO_PORT = settings.MONGO_PORT
        return MongoClient('mongodb://' + MONGO_URL + ':' + MONGO_PORT + '/').mastermind
