from src.exceptions.repeated_username_exception import RepeatedUserNameException
from src.models.game import Game
from src.models.player import Player
from src.repository.game_repository import GameRepository
from src.repository.player_repository import PlayerRepository


class CreateGameService:

    def __init__(self):
        self.game_repository = GameRepository()
        self.player_repository = PlayerRepository()
        self.welcome_msg = "Game created. Available inputs: Red(R), Blue(B), Yellow(Y), Green(G), White(W), Purple(P)"

    def create_game(self, username):
        current_player = self.player_repository.find_player_by_username(username)
        if current_player is not None:
            raise RepeatedUserNameException
        current_player = Player(username)
        self.player_repository.persist(current_player)
        game = Game(player=current_player)
        self.game_repository.persist(game)

        return self.welcome_msg



