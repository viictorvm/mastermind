from src.models.code import Code


class GuessChecker:
    SPECIAL_LETTER = "Z"

    def __init__(self, mastermind, guess: Code):
        self.code = mastermind.secret_code
        self.guess_colours = list(guess.colours)
        self.occurrences = self.numbers_ocurrence()
        self.exact = self.find_matches()
        self.partial = self.find_partial_matches()

    def numbers_ocurrence(self):
        times = {'R': 0, 'B': 0, 'Y': 0, 'G': 0, 'W': 0, 'P': 0}
        for i in range(0, len(self.code)):
            times[self.code[i]] += 1

        return times

    def find_matches(self):
        exact = 0
        for i in range(0, len(self.code)):
            if self.guess_colours[i] == self.code[i]:
                exact += 1
                self.occurrences[self.guess_colours[i]] -= 1
                self.guess_colours[i] = self.SPECIAL_LETTER

        return exact

    def find_partial_matches(self):
        partial = 0
        for i in range(0, len(self.code)):
            if self.guess_colours[i] is not self.SPECIAL_LETTER and self.occurrences[self.guess_colours[i]] > 0:
                partial += 1
                self.occurrences[self.guess_colours[i]] -= 1

        return partial

    def to_json(self):
        return {'guess_colours': self.guess_colours, 'exact': self.exact, 'partial': self.partial}
