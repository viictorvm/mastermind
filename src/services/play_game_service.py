from src.exceptions.username_not_found_exception import UserNameNotFoundException
from src.models.code import Code
from src.repository.attempted_guess_repository import AttemptedGuessRepository
from src.repository.game_repository import GameRepository
from src.repository.player_repository import PlayerRepository


class PlayGameService:

    def __init__(self):
        self.game_repository = GameRepository()
        self.player_repository = PlayerRepository()
        self.attempted_guess_respository = AttemptedGuessRepository()

    def exec_game(self, username, guess):
        current_player = self.player_repository.find_player_by_username(username)
        if current_player is None:
            raise UserNameNotFoundException
        current_game = self.game_repository.find_game_by_player(current_player)
        guess_result = current_game.check(Code(guess))
        if guess_result is None:
            result = {'result': "You are done!"}
        else:
            result = current_game.to_string()
            id_inserted = self.attempted_guess_respository.persist(guess_result)
            self.game_repository.add_attempted_guess_by_player(current_player, id_inserted, current_game.found)

        return result
