from src.exceptions.username_not_found_exception import UserNameNotFoundException
from src.repository.attempted_guess_repository import AttemptedGuessRepository
from src.repository.game_repository import GameRepository
from src.repository.player_repository import PlayerRepository


class HistoricGameService:

    def __init__(self):
        self.game_repository = GameRepository()
        self.player_repository = PlayerRepository()
        self.attempted_guess_respository = AttemptedGuessRepository()

    def find_historic_game_by_username(self, username):
        attempt_collection_array = []
        current_player = self.player_repository.find_player_by_username(username)
        if current_player is None:
            raise UserNameNotFoundException
        current_game = self.game_repository.find_game_by_player(current_player)
        if current_game.attempted_guess:
            for id_attempt in current_game.attempted_guess:
                attempted_guess_row = self.attempted_guess_respository.find_attempted_guess_by_id(id_attempt)
                attempt_collection_array.append(attempted_guess_row.to_json())

        return {'username': username, 'historic game': attempt_collection_array}
