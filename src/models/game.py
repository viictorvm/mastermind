from src.exceptions.username_not_found_exception import UserNameNotFoundException
from src.services.guess_checker_service import GuessChecker
from src.models.code import Code
from src.models.player import Player


class Game:
    TRIES = 10

    def __init__(self, player: Player=None, attempts=0, secret_code=None, attempted_guess=None, found=False):
        self.found = found
        self.attempts = attempts
        self.player = self.__set_player(player)
        self.secret_code = self.__set_secret_code(secret_code)
        self.attempted_guess = self.__set_attempted_guess(attempted_guess)

    def __set_player(self, player: Player):
        if player is None:
            raise UserNameNotFoundException
        return player

    def __set_secret_code(self, code):
        return Code.random() if code is None else code

    def __set_attempted_guess(self, attempted_guess):
        return [] if attempted_guess is None else attempted_guess

    def has_more_attempts(self):
        return self.attempts < self.TRIES

    def has_been_found(self):
        return self.found

    def can_play(self):
        return not self.has_been_found() and self.has_more_attempts()

    def check(self, guess: Code):
        if self.can_play():
            result = GuessChecker(mastermind=self, guess=guess)
            self.attempts += 1
            self.found = len(self.secret_code) == result.exact
            self.attempted_guess.append(result)
            return result
        return None

    def to_string(self):
        return {'username': self.player.username, 'found': self.found, 'lifes': self.TRIES - self.attempts}

    def to_json(self):
        return {'username': self.player.username, 'secret_code': self.secret_code.colours,
                'attempted_guess': self.attempted_guess, 'attempts': self.attempts, 'found': self.found}
