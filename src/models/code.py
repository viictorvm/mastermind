from random import sample

from src.exceptions.colour_not_available_required_exception import ColourNotAvailableException
from src.exceptions.guess_colours_required_exception import ColorInputRequiredException


class Code:
    CODE_SIZE = 4
    AVAILABLES_COLOURS = ['R', 'B', 'Y', 'G', 'W', 'P']

    def __init__(self, colours: str):
        self.colours = self.set_guess_colours(colours)

    @staticmethod
    def random():
        return Code(sample(Code.AVAILABLES_COLOURS, Code.CODE_SIZE))

    def to_json(self):
        return {'code': self.colours}

    def colors(self):
        return self.colours

    def set_guess_colours(self, guess):
        if not guess or len(guess) != self.CODE_SIZE:
            raise ColorInputRequiredException
        guess_colours = list(guess)
        for current_colour in guess_colours:
            if current_colour not in self.AVAILABLES_COLOURS:
                raise ColourNotAvailableException
        return guess_colours
