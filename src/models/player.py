from src.exceptions.username_required_exception import UserNameRequiredException
from src.utils.is_empty import is_empty


class Player:
    username = None

    def __init__(self, username=None):
        self.username = self.__set_username(username)

    def to_json(self):
        return {'username': self.username}

    def __set_username(self, name):
        if is_empty(name):
            raise UserNameRequiredException
        return name
