
class AttemptedGuess:

    def __init__(self, guess_colour, exact: int, partial: int):
        self.guess_code = guess_colour
        self.partial = partial
        self.exact = exact

    def to_json(self):
        return {'guess_code': self.guess_code, 'black': self.exact, 'white': self.partial}
