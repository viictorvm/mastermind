import tornado.ioloop
import tornado.web
from tornado.options import define, options, parse_command_line
from settings import API_PORT

from src.handlers.create_game_handler import CreateGameHandler
from src.handlers.historic_game_handler import HistoricGame
from src.handlers.not_found_handler import NotFoundHandler
from src.handlers.play_game_handler import PlayGameHandler

define("port", default=API_PORT, help="run on the given port", type=int)


class ApplicationTornado(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/play', PlayGameHandler),
            (r'/createGame', CreateGameHandler),
            (r'/historic/([^/]+)', HistoricGame),
            (r'/(.*)', NotFoundHandler),
        ]
        tornado.web.Application.__init__(self, handlers)


if __name__ == '__main__':
    app = ApplicationTornado()
    parse_command_line()
    app.listen(options.port)
    tornado.autoreload.start()
    tornado.ioloop.IOLoop.instance().start()
